import { TreeNode } from 'primeng/api/treenode';

export class FileSystemNode implements TreeNode {
    _name: String;
    _val: number = 0;
    _ratio: number = 100;
    _total: number = 0;
    _fileType: String;
    _visited: boolean;
    data?: FileSystemNode;
    children?: TreeNode[];
    parent?: TreeNode;
    leaf: boolean;
    type: string;
    key: string;

    constructor(data?: any, parent?: TreeNode) {
        if (parent) this.parent = parent;
        if (!data) return;

        let queue = [data];
        let nodeQueue: Array<TreeNode> = [this];

        while (queue.length > 0) {
            let dataNode = queue.shift();
            dataNode.visited = true; // Marking node as visited

            let node = nodeQueue.shift();
            node.data = new FileSystemNode();
            node.data._name = dataNode.data.name;
            node.data._val = dataNode.data.val;
            node.data._fileType = dataNode.data.fileType;
            if (node.parent) {
                node.data.parent = node.parent;
                node.parent = null;
                node.data._ratio = (dataNode.data.val / node.data.parent.data.val) * 100;
            } else {
                node.data._ratio = 100;
            }

            if (!dataNode.children) continue;
            node.data.children = [];
            node.children = [];
            for (let i = 0; i < dataNode.children.length; i++) {
                if (!dataNode.children[i].visited) {
                    queue.push(dataNode.children[i]);
                    let child = new FileSystemNode(null, node);
                    node.children.push(child);
                    node.data.children.push(child); // Added this for root node children reference.
                    nodeQueue.push(node.children[i]);
                }
            }
        }
    }

    get fileType(): String {
        return this._fileType;
    }

    set fileType(val: String) {
        this._fileType = val;
    }

    get total(): number {
        return this._total;
    }

    set total(total: number) {
        this._total = total;
    }

    _factorizeChildrens() {
        // Distributing the values according to the existing child proportion
        // Distributing the values to the child nodes recursively using BFS.
        for (let i = 0; i < this.children.length; i++) {
            let queue = [this.children[i]];
            while (queue.length > 0) {
                let node = <FileSystemNode>queue.shift();
                if (!node.data) continue;
                // Updating the child node value
                if (node.parent) {
                    node.data._val = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)._val;
                } else if (node.data.parent) {
                    node.data._val = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)._val;
                }
                if (!node.data.children) continue;
                for (let j = 0; j < node.data.children.length; j++) {
                    if (!(<FileSystemNode>node.data.children[j])._visited) {
                        queue.push(node.data.children[j]);
                    }
                }
            }
        }
    }

    _factorizeParentAndChildrens() {
        let parent = this.parent;
        let queue = [this.parent];
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
                sum += n.data._val;
                return sum;
            }, sum);
            node.data._val = total;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    node.data._ratio = (node.data._val / node.parent.data._val) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            }
        }


    }

    _factorizeParentAndChildrensMiddle() {
        let parent = this.parent;
        let queue = [this.parent];
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
                sum += n.data._val;
                return sum;
            }, sum);
            node.data._val = total;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }
        }

        for (let i = 0; i < this.children.length; i++) {
            let queue = [this.children[i]];
            while (queue.length > 0) {
                let node = <FileSystemNode>queue.shift();
                if (!node.data) continue;
                // Updating the child node value
                if (node.parent) {
                    node.data._val = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)._val;
                } else if (node.data.parent) {
                    node.data._val = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)._val;
                }
                if (!node.data.children) continue;
                for (let j = 0; j < node.data.children.length; j++) {
                    if (!(<FileSystemNode>node.data.children[j])._visited) {
                        queue.push(node.data.children[j]);
                    }
                }
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    if (!node.parent) continue;
                    node.data._ratio = (node.data._val / node.parent.data._val) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            }
        }
    }

    set val(val: number) {
        this._val = Number(val);
        // If there is no parent then its a root
        if (!this.parent) {
            this._factorizeChildrens();
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens();
            } else {
                this._factorizeParentAndChildrensMiddle();
            }
        }
    }

    get val(): number {
        return this._val;
    }

    get ratio(): number {
        return Number(this._ratio.toFixed(2));
    }

    set ratio(ratio: number) {
        this._ratio = ratio;
    }

    get name(): String {
        return this._name;
    }

    set name(val: String) {
        this._name = val;
    }
}