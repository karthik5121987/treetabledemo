import { ChangeDetectorRef, Component, OnInit, Input } from '@angular/core';
import { FileSystemNode } from '../file-system-node';
import { NodeService } from '../node.service';

@Component({
  selector: 'app-tree-table-demo',
  templateUrl: './tree-table-demo.component.html',
  styleUrls: ['./tree-table-demo.component.scss']
})
export class TreeTableDemoComponent implements OnInit {

  files: FileSystemNode[];

  cols: any[];
  editableCols: any[];

  constructor(private nodeService: NodeService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {

    this.nodeService.getFilesystem().then(files => {
      this.files = files;
    });

    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'val', header: 'Size' },
      { field: 'ratio', header: 'Ratio' },
      { field: 'fileType', header: 'Type' },
    ];

  }

  change() {
    // this.files[0].children[0].data.name = "XYZ";
    console.log(JSON.stringify(this.files, this.replacer));
  }

  replacer(key, val) {
    if (key == "_parent") return undefined;
    else return val;
  }

}
