import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileSystemNode } from './file-system-node';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private http: HttpClient) { }

  getFilesystem() {
    return this.http.get('assets/data/filesystem1.json')
      .toPromise()
      .then(res => {
        let files: FileSystemNode[] = [];
        for (let i = 0; i < (<any>res).data.length; i++) {
          let rootObj = (<any>res).data[i];
          let node = new FileSystemNode(rootObj);
          files.push(node);
        }
        return files;
      });
  }
}
